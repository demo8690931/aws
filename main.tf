# Provider

provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

# VPC

resource "aws_vpc" "demo_vpc" {
  cidr_block = "192.168.0.0/16"
  tags = {
    Name = "demo"
  }
}

# Subnets

resource "aws_subnet" "demo_vpc_public_subnet_1" {
  vpc_id              = aws_vpc.demo_vpc.id
  cidr_block          = "192.168.1.0/24"
  availability_zone   = "${var.region}a"
  tags = {
    Name = "demo_public"
  }
}

resource "aws_subnet" "demo_vpc_private_subnet_1" {
  vpc_id              = aws_vpc.demo_vpc.id
  cidr_block          = "192.168.2.0/24"
  availability_zone   = "${var.region}a"
  tags = {
    Name = "demo_private"
  }
}

# Internet gateway

resource "aws_internet_gateway" "demo_igw" {
  vpc_id = aws_vpc.demo_vpc.id
  tags = {
    Name = "demo_igw"
  }
}

# NAT gateway

resource "aws_eip" "demo_nat_eip" {
  domain = "vpc"
}

resource "aws_nat_gateway" "demo_nat" {
  allocation_id = aws_eip.demo_nat_eip.id
  subnet_id     = aws_subnet.demo_vpc_public_subnet_1.id
  tags = {
    Name = "demo_NAT"
  }
}

# Route table

resource "aws_route_table" "demo_public_route_table" {
  vpc_id = aws_vpc.demo_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo_igw.id
  }

  tags = {
    Name = "demo_public_route_table"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.demo_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.demo_nat.id
  }

  tags = {
    Name = "demo_private_route_table"
  }
}

resource "aws_route_table_association" "demo_public_subnet_association" {
  subnet_id      = aws_subnet.demo_vpc_public_subnet_1.id
  route_table_id = aws_route_table.demo_public_route_table.id
}

resource "aws_route_table_association" "demo_private_subnet_association" {
  subnet_id      = aws_subnet.demo_vpc_private_subnet_1.id
  route_table_id = aws_route_table.private_route_table.id
}

#Prefix list

resource "aws_ec2_managed_prefix_list" "demo_prefix_list" {
  name           = "Demo Whitelist IP HTTP"
  address_family = "IPv4"
  max_entries    = 5

  entry {
    cidr         = "118.189.0.0/16"
  }

  entry {
    cidr         = "116.206.0.0/16"
  }

  entry {
    cidr         = "223.25.0.0/16"
  }
}

# Security group

resource "aws_security_group" "demo_sg" {
  name        = "demo_sg"
  description = "demo_sg"
  vpc_id      = aws_vpc.demo_vpc.id

  ingress {
    description      = "HTTP from employer"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    prefix_list_ids  = [aws_ec2_managed_prefix_list.demo_prefix_list.id]
  }

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "demo_sg"
  }
}

# EC2

resource "aws_instance" "demo_ec2" {
  ami                     = "ami-0261755bbcb8c4a84"
  instance_type           = "t2.micro"
  subnet_id               =  aws_subnet.demo_vpc_private_subnet_1.id
  vpc_security_group_ids  = [aws_security_group.demo_sg.id]
  key_name                = aws_key_pair.demo_keypair.id

  user_data = "${file("docker.sh")}"

  tags = {
    Name = "demo_ec2"
  }
}

# Keypair

resource "aws_key_pair" "demo_keypair" {
  key_name   = "demo"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCyRBhSQpUDG1VYOKx0f9kyATFmoZQ7c73UBzHTV2PnUvo7KE6+7JBTTIDTM4AbWxqJ5uPC9Ua5PQks1p95JRYuSBD+k5hvqdUtp7wpgLWDiM6d21uroyLloyIgOMJJMQ/HN7odhaUuG/KolaO01n/CGzNUO/zEaS8Ck/NDTGIN2cac+7VvKTigQwvC0SfFclZMsv7AHAGhsncvI5b0GtxE9laIvjem1DlkeSIQLW7uLESxs2c0j47z/jKR7FTtyDCHDcI5InIXh2uzUeywG0AiZ26c3xZN4Z+uRYpoeFJeHUVWr4+2ZSHaMg4qngRo7G0Wb2B4Rb3s/BFlAD4J1rrhi/DQX9MV9ejo9Sr9mWGsWhG1PvkB8TEw9IjvnerUeWf2VjZpRyChi1OCV8irp3Kyof1I99tfEQWnvUW8SIUjX7DyXr1z3CyPIMVkNPbPr7VZa8f4fMNwwVDk0EuSePZLxdRiurmPi1dntKkRe77pVNDyDhZ+iKVF6/iO+37C8dk= terraform"
}

# network load balancer

resource "aws_lb" "demo_nlb" {
  name               = "demo-nlb"
  internal           = false
  load_balancer_type = "network"
  subnets            = [aws_subnet.demo_vpc_public_subnet_1.id]
}

resource "aws_lb_listener" "nlb_ssh_listener" {
  load_balancer_arn = aws_lb.demo_nlb.arn
  port              = 22
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.demo_ssh_tg.arn
  }
}

resource "aws_lb_listener" "nlb_http_listener" {
  load_balancer_arn = aws_lb.demo_nlb.arn
  port              = 80
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.demo_http_tg.arn
  }
}

resource "aws_lb_target_group" "demo_ssh_tg" {
  name        = "demo-ssh-tg"
  port        = 22
  protocol    = "TCP"
  vpc_id      = aws_vpc.demo_vpc.id
}

resource "aws_lb_target_group" "demo_http_tg" {
  name        = "demo-http-tg"
  port        = 80
  protocol    = "TCP"
  vpc_id      = aws_vpc.demo_vpc.id
}

resource "aws_lb_target_group_attachment" "demo_ssh_instance_attachment" {
  target_group_arn = aws_lb_target_group.demo_ssh_tg.arn
  target_id        = aws_instance.demo_ec2.id
  port             = 22
}

resource "aws_lb_target_group_attachment" "demo_http_instance_attachment" {
  target_group_arn = aws_lb_target_group.demo_http_tg.arn
  target_id        = aws_instance.demo_ec2.id
  port             = 80
}

# IAM

resource "aws_iam_user" "demo" {
  name = "demo"
}

resource "aws_iam_user_policy_attachment" "demo_iam_policy" {
  user       = aws_iam_user.demo.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_user_login_profile" "demo_login_profile" {
  user     = aws_iam_user.demo.name
  pgp_key = "keybase:demo29582"
}

output "password" {
  value = aws_iam_user_login_profile.demo_login_profile.encrypted_password
}

#to decrypt password
#terraform output password | base64 -d | keybase pgp decrypt
#got bug in PS > use cmd to run