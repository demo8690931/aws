#!/bin/bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt install docker-ce -y
sudo systemctl start docker
sudo systemctl enable docker
cat <<EOT > /home/ubuntu/demo_nginx.conf
server {
    listen 80;

location / {
    default_type text/html;
    return 200 "<!DOCTYPE html><h1>Adrian</h1>\n";
}
}
EOT
sudo docker run -d --name demo_nginx -p 80:80 -v /home/ubuntu/demo_nginx.conf:/etc/nginx/conf.d/default.conf nginx:1.18.0